import React from 'react';
import RootStack from './app/navigation/RootStack';
import { Notifications, Permissions, Constants } from 'expo'
import moment from 'moment'; // 2.22.2

export default class App extends React.Component {
    async componentDidMount() {
        let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

        if (Constants.isDevice && result.status === 'granted') {
            console.log('Notification permissions granted.')
        }

        Notifications.addListener(this._handleNotification);
        this._sendDelayedNotification();
    }
    _handleNotification = ({ origin, data }) => {
        console.info(`Notification (${origin}) with data: ${JSON.stringify(data)}`)
    }
    _sendDelayedNotification () {
        const localNotification = {
            title: 'ZodoScope',
            body: `Check your today's horoscope now`,
            data: { type: 'delayed' },
        }
        const schedulingOptions = {
            time: (new Date()).getTime() + 5000,
            repeat:'day'
        }

        console.log('Scheduling delayed notification:', { localNotification, schedulingOptions })

        Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions)
            .then(id => console.info(`Delayed notification scheduled (${id}) at ${moment(schedulingOptions.time).format()}`))
            .catch(err => console.error(err))
    }
    render() {
        return <RootStack />;
    }
}
