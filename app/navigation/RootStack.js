import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import { createStackNavigator } from 'react-navigation';

const RootStack = createStackNavigator(
    {
        HomeScreen: {
            screen: HomeScreen
        },
        DetailScreen: {
            screen: DetailScreen
        },
    },
    {
        initialRouteName: 'HomeScreen',
        headerMode: 'screen',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#006ede',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                textAlign: 'center',
                alignSelf:'center'
            },
        },
    }
);

export default RootStack;