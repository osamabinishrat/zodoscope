const Color = {
    white: '#ffffff',
    error: '#f44336',
    facebook: '#3b5998',
    google: '#d34836',
    primaryDark: '#226487',
    primary: '#247BA0',
    primaryMedium: '#70C1B3',
    primaryLight: '#B2DBBF',
    secondary: '#FF1654',
    accent: '#FFC107',
    accentLight: '#FFD54F',
    gray: '#666666',
    lightGray: '#a0a0a0',
    checkGreen: '#00ca71',
    lightgray: '#d1d1d1',
    verylightgray: '#e9eef2',
    grayTitle:'#818890',
    darkGray: '#3c3c3c',
    toastDefault: '#515151',
    stepActive: '#a7000b',
    stepInActive: 'rgba(207, 212, 216, 0.8)',
    blackTextPrimary: 'rgba(0,0,0,1)',
    blackTextSecondary: 'rgba(0,0,0,0.5)',
    blackTextDisable: 'rgba(0,0,0,0.3)',

    lightTextPrimary: 'rgba(255,255,255,1)',
    lightTextSecondary: 'rgba(255,255,255,255.5)',
    lightTextDisable: 'rgba(255,255,255,0.3)',

    lightDivide: 'rgba(255,255,255,0.12)',
    blackDivide: 'rgba(0,0,0,0.05)',

    background: 'white',

    Background: '#FFFFFF',
    DirtyBackground: '#F0F0F0',
    Error: '#ff0033',
    LightBackground:'#e9eef2',
    //Toolbar
    Toolbar: 'white',
    ToolbarText: '#283747',
    ToolbarIcon: '#283747',

    ToolbarTrans: 'transparent',
    ToolbarTextTrans: 'black',
    ToolbarIconTrans: 'black',

    TopBar: 'white',
    TopBarIcon: '#283747',

    //Button
    ButtonBackground: '#d41e36',
    ButtonText: 'white',
    ButtonBorder: '#bcbebb',

    //Product tabs
    TabActive: '#a7000b',
    TabDeActive: 'white',
    TabActiveText: '#333',
    TabText: '#333',
    BuyNowButton: '#d41e36',

    ViewBorder: '#bcbebb',

    //Text
    Text: '#333',
    TextNormal: '#77a464',
    TextLight: 'darkgray',
    TextDark: '#000000',
    TextWhite: '#ffffff',

    //sidemenu
    SideMenu: '#34BC99',
    SideMenuText: 'white',
    SideMenuIcon: 'white',

    // bottom tab bar
    tabbar: 'rgba(255, 255, 255, 1)',
    tabbarTint: "#d4535a",
    tabbarColor: '#ccc',

    //navigation bar
    headerTintColor: "#d4535a",
    navigationBarColor: "#F6F6F8",
    navigationBarIcon: "rgba(0, 0, 0, 0.3)",
    navigationTitleColor: "rgba(0, 0, 0, 0.8)",

    //wishlist
    heartActiveWishList: "rgba(252, 31, 74, 1)",

    spin: '#333333',

    attributes: {
        black: '#333',
        red: '#DF3737',
        green: '#9BC53D',
        blue: '#38B1E7',
        yellow: '#FDF12C',
    },
};

export default Color;
