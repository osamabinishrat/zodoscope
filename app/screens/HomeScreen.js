import React from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import GridView from 'react-native-super-grid';
import {View as ShoutemView} from "@shoutem/ui";
import {AdMobBanner} from 'expo';

const items = [
    { name: 'ARIES', code: '#1abc9c', image: require('../../assets/aries.png'), background:require('../../assets/aries-gradiant.png') },
    { name: 'TAURUS', code: '#ff7d50', image: require('../../assets/taurus.png'), background:require('../../assets/taurus-gradiant.png') },
    { name: 'GEMINI', code: '#3498db', image: require('../../assets/gemini.png'), background:require('../../assets/gemini-gradiant.png') },
    { name: 'CANCER', code: '#ff50d4', image: require('../../assets/cancer.png'), background:require('../../assets/cancer-gradiant.png') },
    { name: 'LEO', code: '#f39c12', image: require('../../assets/leo.png'), background:require('../../assets/leo-gradiant.png') },
    { name: 'VIRGO', code: '#16a085', image: require('../../assets/virgo.png'), background:require('../../assets/virgo-gradiant.png') },
    { name: 'LIBRA', code: '#27ae60', image: require('../../assets/libra.png'), background:require('../../assets/libra-gradiant.png') },
    { name: 'SCORPIO', code: '#2980b9', image: require('../../assets/scorpio.png'), background:require('../../assets/scorpio-gradiant.png') },
    { name: 'SAGITTARIUS', code: '#ff5672', image: require('../../assets/sagittarius.png'), background:require('../../assets/sagittarius-gradiant.png') },
    { name: 'CAPRICORN', code: '#f1c40f', image: require('../../assets/capricorn.png'), background:require('../../assets/capricorn-gradiant.png') },
    { name: 'AQUARIUS', code: '#e74c3c', image: require('../../assets/aquarius.png'), background:require('../../assets/aquarius-gradiant.png') },
    { name: 'PISCES', code: '#34495e', image: require('../../assets/pisces.png'), background:require('../../assets/pisces-gradiant.png') },
];
export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'ZodoScope',
    };
    render() {
        return (
            <ShoutemView styleName={"vertical fill-parent"}>
            <GridView
                itemDimension={130}
                items={items}
                style={styles.gridView}
                renderItem={item => (
                    <TouchableOpacity onPress={(e)=>{
                        this.props.navigation.navigate('DetailScreen',{
                            title:item.name,
                            background:item.background
                        })
                    }}>
                        <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                            <Image source={item.image} style={styles.image} />
                            <Text style={styles.itemName}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            />
                <AdMobBanner
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-1127768322529051/4237682629" // Test ID, Replace with your-admob-unit-id
                    testDeviceID="EMULATOR" />
            </ShoutemView>
        );
    }
}
const styles = StyleSheet.create({
    gridView: {
        paddingTop: 25,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'flex-end',
        alignItems:'center',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    itemName: {
        fontSize: 20,
        textAlign:'center',
        color: '#fff',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
    image:{
        width: 100, height: 100
    }
});