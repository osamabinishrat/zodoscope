import React from "react";
import {Dimensions, StyleSheet} from 'react-native';
import {Image as ShoutemImage, View as ShoutemView} from '@shoutem/ui';
import {Container, Tab, Tabs} from 'native-base';
import HoroscopeComponent from "../components/HoroscopeComponent";

const win = Dimensions.get('window');

export default class DetailScreen extends React.Component {
    static navigationOptions = { header: null };
    render() {
        let {title,background} = this.props.navigation.state.params;
        // let title = 'LEO';
        return (
            <ShoutemView style={{flex:1}} styleName={'vertical h-start v-start space-between'}>
                <ShoutemView styleName={'horizontal h-start v-start'}>
                    <ShoutemImage styleName={'large-wide'} source={background} />
                </ShoutemView>
                <ShoutemView styleName={'vertical h-start v-start wrap'}>
                    <Container>
                        <Tabs initialPage={0} tabStyle={{backgroundColor: '#006ede'}}>
                            <Tab heading="Today">
                               <HoroscopeComponent sign={title} query={'today'} />
                            </Tab>
                            <Tab heading="This Week">
                                <HoroscopeComponent sign={title} query={'week'} />
                            </Tab>
                            <Tab heading="This Month">
                                <HoroscopeComponent sign={title} query={'month'} />
                            </Tab>
                            <Tab heading="This Year">
                                <HoroscopeComponent sign={title} query={'year'} />
                            </Tab>
                        </Tabs>
                    </Container>
                </ShoutemView>
            </ShoutemView>
        );
    }
}
const styles = StyleSheet.create({
    image: {
        width: win.width,
        height:230,
        alignSelf:'stretch'
    }
});