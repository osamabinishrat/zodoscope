import React from "react";
import {ScrollView, Text, View} from 'react-native';
import colors from '../constants/colors';
import { Container, Content, Spinner } from 'native-base';
import Moment from 'moment';
import {AdMobBanner} from 'expo';

export default class HoroscopeComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date:'',
            horoscope:'',
            loading:true
        };
    }
    componentWillMount(){
        let {sign,query} = this.props;
        let self = this;
        fetch(`http://horoscope-api.herokuapp.com/horoscope/${query}/${sign}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                let horoscope = responseJson.horoscope.split("[").join("");
                horoscope = horoscope.split("]").join("");
                horoscope = horoscope.split("Ganesha").join("ZodoScope");
                let date = responseJson[query];
                if(query==='today') {
                    date = Moment(responseJson.date,"DD-MM-YYYY").format('d MMM YYYY')
                }
                self.setState({
                    horoscope: horoscope ,
                    loading:false,
                    date: date
                });
                return horoscope;
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        let {date,horoscope,loading} = this.state;
        if(loading)
            return (
                <Container>
                    <Content>
                        <Spinner color='blue' />
                    </Content>
                </Container>
            );
        return (
            <View style={{padding:20,paddingBottom:260}}>
                {/*<Text style={{fontSize:25,fontWeight:'bold',color:colors.lightGray}}>{date}</Text>*/}
                <ScrollView>
                    <Text style={{fontSize:18,color:colors.darkGray}}>{horoscope}</Text>
                </ScrollView>
                <AdMobBanner style={{position:'absolute',bottom:0,left:0}}
                    bannerSize="smartBannerPortrait"
                    adUnitID="ca-app-pub-1127768322529051/4237682629" // Test ID, Replace with your-admob-unit-id
                    testDeviceID="EMULATOR" />
            </View>
        );
    }
}
